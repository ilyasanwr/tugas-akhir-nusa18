import BreadCrumb from 'components/BreadCrumbs/BreadCrumbs'
import ButtonGrid from 'components/Button/Buttons'
import LeatsTalkComp from 'components/LetsTalk/LetsTalkComp'
import HeroDetailwork from 'components/hero-detailwork/HeroDetailwork'
import ParagrafDetailwork from 'components/paragraf-detailwork/ParagrafDetailwork'
import SwiperDetail from 'components/swiperDetail/SwiperDetail'
// import SwiperDetail from 'components/swiperDetail/SwiperDetail'
import Footer from 'layouts/containers/Public/Footer'
import Header from 'layouts/containers/Public/Header'
import React from 'react'

function OurworkDetail() {
  const breadcrumbItems = [
    { text: 'Our Works', link: '/ourworks' },
    { text: 'Trimegah Dashboard' },
  ]
  return (
    <>
      {/* herosection of the project */}
      <div className="mx-[140px]">
        {/* button to go back home page */}
        <div className="my-[50px]">
          <BreadCrumb items={breadcrumbItems} />
        </div>
        {/* herosection of the project */}
        <HeroDetailwork
          Title="Trimegah Dashboard"
          Description="Web Design / Web Development"
          Image="./static/images/content/ourWorks/hero-detailwork.svg"
          NameLink="Trimegah Dashboard"
        />
        {/* content of the project */}
        <div className="flex items-center justify-between">
          <ParagrafDetailwork
            subTitle="What we do"
            title="Project Overview"
            paragraf="Lorem ipsum dolor sit amet consectetur. Enim dolor senectus in
              gravida lectus pretium nulla eu. Nibh nullam sit at neque orci est
              id. Consequat ut nunc justo morbi. Mi leo tortor eleifend mauris
              aliquam venenatis erat parturient. Eu aliquam maecenas a sed amet
              ipsum eleifend semper. Sed arcu aenean orci in purus orci."
            styleParagraf="text-[#404258] text-[16px] w-[582px] font-[400px] leading-[1.8]"
          />
          <div>
            <img
              src="./static/images/content/ourWorks/hero-portfolio.svg"
              alt="statiska"
            />
          </div>
        </div>
        <ParagrafDetailwork
          subTitle="What we do"
          title="Our Responsibility"
          paragraf="Lorem ipsum dolor sit amet consectetur. Sem tincidunt aliquam fames lectus mattis mi imperdiet euismod sodales. In pulvinar commodo et id eu imperdiet. Congue et condimentum ullamcorper commodo dictumst. Nisl commodo augue ac nulla. Malesuada nulla nulla dictum pretium. Nam tincidunt proin et sit. Vivamus sit sed lorem at varius. Massa odio tristique interdum massa pellentesque consequat phasellus ultrices. At arcu eu posuere rhoncus senectus. Malesuada rutrum accumsan quis nulla ipsum tellus feugiat montes vulputate. Fringilla tristique sed rhoncus proin neque lectus non. Blandit dictum porta lectus urna eu sed."
          styleParagraf="text-[#404258] text-[16px] font-[400px] leading-[1.8]"
        />
        <div className="flex flex-wrap gap-x-[35px] mb-[60px]">
          <ButtonGrid NameButton="wireframe" />
          <ButtonGrid NameButton="UI/UX Design" />
          <ButtonGrid NameButton="Project Management" />
          <ButtonGrid NameButton="Server Dev Ops" />
          <ButtonGrid NameButton="Web Development" />
          <ButtonGrid NameButton="Front-End" />
          <ButtonGrid NameButton="Back-End" />
          <ButtonGrid NameButton="Technology Advisor" />
          <ButtonGrid NameButton="Graphic Design" />
        </div>
        <div>
          <ParagrafDetailwork subTitle="What we do" title="Tehcnology Stack" />
          <div className="flex gap-x-20 text-center mt-10 mb-[60px]">
            <span>
              <img
                src="./static/images/icons/html-icon.svg"
                alt="icon-html"
                className="mb-[15px]"
              />
              <h4>HTML</h4>
            </span>
            <span>
              <img
                src="./static/images/icons/css3-icon.svg"
                alt="icon-css3"
                className="mb-[15px]"
              />
              <h4>CSS 3</h4>
            </span>
            <span>
              <img
                src="./static/images/icons/js-icon.svg"
                alt="icon-js"
                className="mb-[15px]"
              />
              <h4>Javascript</h4>
            </span>
            <span>
              <img
                src="./static/images/icons/react-icon.svg"
                alt="icon-react"
                className="mb-[15px]"
              />
              <h4>React</h4>
            </span>
            <span>
              <img
                src="./static/images/icons/laravel-icon.svg"
                alt="icon-larevel"
                className="mb-[15px]"
              />
              <h4>Larevel</h4>
            </span>
            <span>
              <img
                src="./static/images/icons/mysql-icon.svg"
                alt="icon-mysql"
                className="mb-[15px]"
              />
              <h4>MySQL</h4>
            </span>
          </div>
        </div>
        <div className="mb-[121px]">
          <ParagrafDetailwork title="Gallery" />
          <img
            src="./static/images/content/ourWorks/tri1-gallery.svg"
            alt="gallery"
            className="rounded-3xl w-[949px] mt-5 mx-auto"
          />
        </div>
      </div>
      <div className="bg-[#F8F8F8] w-full">
        <div className="px-24 py-20">
          <SwiperDetail
            sebelumText="SIPENCATAR"
            berikutnyaText="SIPRUS KEMENPUPR"
          />
        </div>
        <div className="mb-[120px]">
          <LeatsTalkComp />
        </div>
      </div>
      {/* footer of the project */}
      <Footer />
    </>
  )
}

export default OurworkDetail
